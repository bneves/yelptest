//
//  BusinessCell.swift
//  Test_Bruno_Neves
//
//  Created by BRUNO NEVES on 03/05/16.
//  Copyright © 2016 BRUNO NEVES. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage
import Cosmos

class BusinessCell: UITableViewCell {
     
     @IBOutlet weak var businessLabel: UILabel!
     @IBOutlet weak var businessDistanceLabel: UILabel!
     @IBOutlet weak var businessCategoriesLabel: UITextView!
     @IBOutlet weak var businessThumb: UIImageView!
     @IBOutlet weak var cosmosRating:CosmosView!
     @IBOutlet weak var contentInfo: UIView!

     var business: Business!{
          
          didSet {
          
               self.businessLabel.text = business.name!
               self.businessDistanceLabel.text = business.distance!
            
               if business.categories != nil {
                    self.businessCategoriesLabel.text = business.categories!
               }
               
               self.cosmosRating.rating = Double(business.rating!)
               
               if business.image_url != nil  {
                    self.businessThumb.af_setImageWithURL(
                         NSURL(string: business.image_url!)!,
                         placeholderImage: nil,
                         filter: nil,
                         imageTransition: .CrossDissolve(0.5),
                         completion: { response in
                             
                         }
                    )
               }
          }
     }
     
     override func awakeFromNib() {
          super.awakeFromNib()
          self.contentView.clipsToBounds = true
          
          // Initialization code
     }
     
     override func setSelected(selected: Bool, animated: Bool) {
          super.setSelected(selected, animated: animated)
          
          // Configure the view for the selected state
     }
     
}
