//
//  BusinessDetailTitleCell.swift
//  Test_Bruno_Neves
//
//  Created by BRUNO NEVES on 04/05/16.
//  Copyright © 2016 BRUNO NEVES. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class BusinessDetailTitleCell: UITableViewCell {
     
     @IBOutlet weak var businessLabel: UILabel!
     @IBOutlet weak var businessDistanceLabel: UILabel!
     @IBOutlet weak var businessCategoriesLabel: UILabel!
     @IBOutlet weak var cosmosRating:CosmosView!
     
     var business: Business!{
          
          didSet {
               self.businessLabel.text = business.address!
               self.businessDistanceLabel.text = business.distance!
               self.businessCategoriesLabel.text = business.categories!
               self.cosmosRating.rating = Double(business.rating!)
          }
     }
     
     override func awakeFromNib() {
          super.awakeFromNib()
          self.contentView.clipsToBounds = true
          
          // Initialization code
     }
     
     override func setSelected(selected: Bool, animated: Bool) {
          super.setSelected(selected, animated: animated)
          
          // Configure the view for the selected state
     }
     
}

