//
//  BusinessDetailThumbCell.swift
//  Test_Bruno_Neves
//
//  Created by BRUNO NEVES on 04/05/16.
//  Copyright © 2016 BRUNO NEVES. All rights reserved.
//

import Foundation
import UIKit

class BusinessDetailThumbCell: UITableViewCell {
     
     @IBOutlet weak var businessThumb: UIImageView!
     @IBOutlet weak var contentInfo: UIView!
     
     var businessThumbURL: String!{
          
          didSet {
               self.businessThumb.af_setImageWithURL(NSURL(string: self.businessThumbURL!)!)
          }
     }
     
     override func awakeFromNib() {
          super.awakeFromNib()
          self.contentView.clipsToBounds = true
          
          // Initialization code
     }
     
     override func setSelected(selected: Bool, animated: Bool) {
          super.setSelected(selected, animated: animated)
          
          // Configure the view for the selected state
     }
     
}

