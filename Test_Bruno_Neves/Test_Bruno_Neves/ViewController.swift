//
//  ViewController.swift
//  Test_Bruno_Neves
//
//  Created by BRUNO NEVES on 03/05/16.
//  Copyright © 2016 BRUNO NEVES. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

struct DefaultCoordenate{
     let latitude:Double = 43.6598036326469
     let longitude:Double = -79.4289836771952
}

enum FilterState {
     case AZ
     case NearMe
}

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UITabBarDelegate,CLLocationManagerDelegate {
     
     @IBOutlet weak var tableViewHeader: UIView!
     @IBOutlet weak var tableView: UITableView!
     
     var businesses: [Business]!
     var business:Business!
     
     var filterState:FilterState? = FilterState.AZ
     
     var searchController:    UISearchController!
     var locationManager: CLLocationManager!
     
     let BusinessCellIdentifier         = "BusinessCellIdentifier"
     let BusinessCellIdentifierLeft     = "BusinessCellLeft"
     let BusinessCellIdentifierRight    = "BusinessCellRight"
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          navigationItem.title = "Yelp!"
          UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState:.Selected)
          
          //Only for try on Simulator
          //Check is Simulator and put Toronto coordinate
          if Platform.isSimulator {
               UserInfo.latitude  = DefaultCoordenate().latitude
               UserInfo.longitude = DefaultCoordenate().longitude
               
               //Searching term on API
               self.searchTerm("Restaurants")
          }
          else {
              self.getCurrentLocation()
          }
     }
     
     //Location delegate
     func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
          
          let location = locations.last! as CLLocation
          
          UserInfo.latitude  = location.coordinate.latitude
          UserInfo.longitude = location.coordinate.longitude
          
          locationManager.stopUpdatingLocation()
          
          //Searching term on API
          self.searchTerm("Restaurants")
          
     }
     
     
     func getCurrentLocation() {
     
          if (CLLocationManager.locationServicesEnabled()) {
               locationManager = CLLocationManager()
               locationManager.delegate = self
               locationManager.desiredAccuracy = kCLLocationAccuracyBest
               locationManager.requestAlwaysAuthorization()
               dispatch_async(dispatch_get_main_queue(), {
                    self.locationManager.startUpdatingLocation()
               })
          }else{
               //Alert!!!!
          }
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     //TableView delegate
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          if businesses != nil {
               return businesses!.count
          }else{
               return 0
          }
     }
     
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
          let cell:BusinessCell = tableView.dequeueReusableCellWithIdentifier(BusinessCellIdentifier) as! BusinessCell
          
          cell.business = businesses[indexPath.row]
          
          return cell
          
     }
     
     func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

           self.business = businesses[indexPath.row]
           self.performSegueWithIdentifier("businessDetailIdentifier", sender: nil)
     }
     
     func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
          
          var index = 0
          
          let customSegmentedControl = UISegmentedControl (items: ["A...Z","Near me"])
         
          if self.filterState == .NearMe {
              index = 1
          }
          
          customSegmentedControl.selectedSegmentIndex = index
          
          customSegmentedControl.tintColor = UIColor(hexString: "#ff4d54")
          customSegmentedControl.backgroundColor = UIColor.whiteColor()
          customSegmentedControl.addTarget(self, action: #selector(ViewController.segmentedValueChanged(_:)), forControlEvents: .ValueChanged)
       
          return customSegmentedControl
     }
     
     func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
          return 45.0
     }
     
     func segmentedValueChanged(sender:UISegmentedControl!)
     {
          if sender.selectedSegmentIndex == 0 {
               self.sortByAZ(self.businesses)
          }else{
               self.sortByNearMe(self.businesses)
          }
     }
     
     //Search terms
     func searchTerm(term:String!){
          Business.searchWithTerm(term, completion: { (businesses: [Business]!, error: NSError!) -> Void in
               
               if self.filterState == .AZ {
                    self.sortByAZ(businesses)
               }else{
                    self.sortByNearMe(businesses)
               }
       
          })
     }
     
     //reload TableView
     func reloadTable(){
          dispatch_async(dispatch_get_main_queue(), {
               self.tableView.reloadData()
               self.scrollToFirstRow()
          })
     }
     
     //Sort by A...Z
     func sortByAZ(businesses: [Business]!){
          let businessesContainer = businesses
          let sortedBusinesses = businessesContainer.sort { $0.name < $1.name }
          
          self.businesses = sortedBusinesses
          
          self.filterState = .AZ
          
          self.reloadTable()
     }
     
     //Sort by NearMe
     func sortByNearMe(businesses: [Business]!){
          let businessesContainer = businesses
          let sortedBusinesses = businessesContainer.sort { $0.distance < $1.distance }
          
          self.businesses = sortedBusinesses
          
          self.filterState = .NearMe
          
          self.reloadTable()
     }
     
     //Parallax effect
     func scrollViewDidScroll(scrollView: UIScrollView) {
          let offsetY = self.tableView.contentOffset.y
          for cell in self.tableView.visibleCells as! [BusinessCell] {
               let x = cell.businessThumb.frame.origin.x
               let w = cell.businessThumb.bounds.width
               let h = cell.businessThumb.bounds.height
               let y = ((offsetY - cell.frame.origin.y) / h) * 100
               cell.businessThumb.frame = CGRectMake(x, y, w, h)
          }
     }
     
     
     //MARK: UISearchBar Delegate
     func searchBarSearchButtonClicked(searchBar: UISearchBar){
 
          searchBar.resignFirstResponder()
          dismissViewControllerAnimated(true, completion: nil)
          
          //Searching term on API
           self.searchTerm(searchBar.text!)
          
     }
     
     //TabBar delegate
     func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
          if item.tag == 0 {
               searchController = UISearchController(searchResultsController: nil)
               searchController.hidesNavigationBarDuringPresentation = false
               self.searchController.searchBar.delegate = self
               presentViewController( searchController, animated: true, completion: nil )
          }
     }
     
     func scrollToFirstRow() {
          let indexPath = NSIndexPath(forRow: 0, inSection: 0)
          self.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Top, animated: false)
     }
     
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
          
          if segue.identifier == "businessDetailIdentifier" {
               let businessDetail:BusinessDetailViewController = segue.destinationViewController as! BusinessDetailViewController
               businessDetail.business = self.business
          }
          
     }

}

