//
//  YelpClient.swift
//  Test_Bruno_Neves
//
//  Created by BRUNO NEVES on 03/05/16.
//  Copyright © 2016 BRUNO NEVES. All rights reserved.
//


import UIKit

import AFNetworking
import BDBOAuth1Manager

struct YelpAPI {
     let api             = "https://api.yelp.com/v2/"
     let api_business    = "https://api.yelp.com/v2/business/"
     let consumerKey     = "89OB1zb-i_zdk56oPS_LoA"
     let consumerSecret  = "5HI3bCD-kqeY0yM1Bordy3kKJts"
     let accessToken     = "4jY03cjd82sOIP9yb4U5B3uPgedLNfa5"
     let accessSecret    = "uPXQjub8lJzkmzZAJ2dGDnQRsn4"
}

struct UserInfo {
     static var latitude:Double?
     static var longitude:Double?
}


enum YelpSortMode: Int {
     case BestMatched = 0, Distance, HighestRated
}

class YelpClient: BDBOAuth1RequestOperationManager {
     
     var accessToken: String!
     var accessSecret: String!
     
     class var sharedInstance : YelpClient {
          struct Static {
               static var token : dispatch_once_t = 0
               static var instance : YelpClient? = nil
          }
          
          dispatch_once(&Static.token) {
               Static.instance = YelpClient(consumerKey: YelpAPI().consumerKey, consumerSecret: YelpAPI().consumerSecret, accessToken: YelpAPI().accessToken, accessSecret: YelpAPI().accessSecret)
          }
          return Static.instance!
     }
     
     required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
     }
     
     init(consumerKey key: String!, consumerSecret secret: String!, accessToken: String!, accessSecret: String!) {
          self.accessToken = accessToken
          self.accessSecret = accessSecret
          
          let baseUrl = NSURL(string: YelpAPI().api)
          
          super.init(baseURL: baseUrl, consumerKey: key, consumerSecret: secret);
          
          let token = BDBOAuth1Credential(token: accessToken, secret: accessSecret, expiration: nil)
          self.requestSerializer.saveAccessToken(token)
     }
     
     func searchWithTerm(term: String, completion: ([Business]!, NSError!) -> Void) -> AFHTTPRequestOperation {
          return searchWithTerm(term, sort: nil, categories: nil, deals: nil, completion: completion)
     }
     
     
     func searchWithTerm(term: String, sort: YelpSortMode?, categories: [String]?, deals: Bool?, completion: ([Business]!, NSError!) -> Void) -> AFHTTPRequestOperation {
          
          let location:String = "\(UserInfo.latitude!),\(UserInfo.longitude!)"
          
          var parameters: [String : AnyObject] = [
               "term": term,
               "ll": location,
               "limit": 12,
               "actionlinks": true
          ]

          if sort != nil {
               parameters["sort"] = sort!.rawValue
          }
          
          if categories != nil && categories!.count > 0 {
               parameters["category_filter"] = (categories!).joinWithSeparator(",")
          }
          
          if deals != nil {
               parameters["deals_filter"] = deals!
          }
          
          return self.GET("search", parameters: parameters, success: { (operation: AFHTTPRequestOperation!, response: AnyObject!) -> Void in
               let dictionaries = response["businesses"] as? [NSDictionary]
               if dictionaries != nil {
                    completion(Business.businesses(array: dictionaries!), nil)
               }
               }, failure: { (operation: AFHTTPRequestOperation?, error: NSError!) -> Void in
                    completion(nil, error)
          })!
     }
     
         
}
