//
//  BusinessReviewTitleCell.swift
//  Test_Bruno_Neves
//
//  Created by BRUNO NEVES on 04/05/16.
//  Copyright © 2016 BRUNO NEVES. All rights reserved.
//

import Foundation
import UIKit

class BusinessReviewTitleCell: UITableViewCell {
     
     @IBOutlet weak var reviewsCountLabel: UILabel!
     
     var business: Business!{
          
          didSet {
               //self.reviewsCountLabel.text = "Reviews \(String(business.review_count!))"
          }
     }
     
     override func awakeFromNib() {
          super.awakeFromNib()
          self.contentView.clipsToBounds = true
          
          // Initialization code
     }
     
     override func setSelected(selected: Bool, animated: Bool) {
          super.setSelected(selected, animated: animated)
          
          // Configure the view for the selected state
     }
     
}

