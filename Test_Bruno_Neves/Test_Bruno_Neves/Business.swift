//
//  Business.swift
//  Test_Bruno_Neves
//
//  Created by BRUNO NEVES on 03/05/16.
//  Copyright © 2016 BRUNO NEVES. All rights reserved.
//

import UIKit


class Business: NSObject {
     
     var id: String?
     var name: String?
     var phone: String?
     var snippet_text:String?
     var address: String?
     var image_url: String?
     var categories: String?
     var distance: String?
     let rating: Int?
     let location: NSDictionary?
     let review_count: NSNumber?

     
     init(dictionary: NSDictionary) {
        
          id = dictionary["id"] as? String ?? "[NO ID]"
          
          name = dictionary["name"] as? String ?? "[NO NAME]"
          
          phone = dictionary["phone"] as? String ?? "[NO PHONE]"
          
          image_url = dictionary["image_url"] as? String
          
          rating = dictionary["rating"] as? Int ?? 0
         
          snippet_text = dictionary["snippet_text"] as? String ?? "[NO SNIPPET]"
          
          location = dictionary["location"] as? NSDictionary ?? [:]
          
          review_count = dictionary["review_count"] as? NSNumber ?? 0

          
          var address = ""
          if location != nil {
               let addressArray = location!["address"] as? NSArray
               if addressArray != nil && addressArray!.count > 0 {
                    address = addressArray![0] as! String
               }
               
               let neighborhoods = location!["neighborhoods"] as? NSArray
               if neighborhoods != nil && neighborhoods!.count > 0 {
                    if !address.isEmpty {
                         address += ", "
                    }
                    address += neighborhoods![0] as! String
               }
          }
          
          self.address = address
          
          //Categories
          let categoriesArray = dictionary["categories"] as? [[String]]
          if categoriesArray != nil {
               var categoryNames = [String]()
               for category in categoriesArray! {
                    let categoryName = category[0]
                    categoryNames.append(categoryName)
               }
               categories = categoryNames.joinWithSeparator(", ")
          } else {
               categories = nil
          }
          
          let distanceMeters = dictionary["distance"] as? NSNumber
          if distanceMeters != nil {
               let milesPerMeter = 0.000621371
               distance = String(format: "%.2f mi", milesPerMeter * distanceMeters!.doubleValue)
          } else {
               distance = nil
          }
     }
     
     class func businesses(array array: [NSDictionary]) -> [Business] {
          var businesses = [Business]()
          for dictionary in array {
               let business = Business(dictionary: dictionary)
               businesses.append(business)
          }
          return businesses
     }
     
     class func searchWithTerm(term: String, completion: ([Business]!, NSError!) -> Void) {
          YelpClient.sharedInstance.searchWithTerm(term, completion: completion)
     }
     
     class func searchWithTerm(term: String, sort: YelpSortMode?, categories: [String]?, deals: Bool?, completion: ([Business]!, NSError!) -> Void) -> Void {
          YelpClient.sharedInstance.searchWithTerm(term, sort: sort, categories: categories, deals: deals, completion: completion)
     }
}
