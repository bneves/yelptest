//
//  businessDetailViewController
//  Test_Bruno_Neves
//
//  Created by BRUNO NEVES on 03/05/16.
//  Copyright © 2016 BRUNO NEVES. All rights reserved.
//

import Foundation
import UIKit


class BusinessDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
     
     @IBOutlet weak var tableView: UITableView!
  
     
     var business: Business!
     
     let BusinessDetailThumbCellIdentifier        = "BusinessDetailThumbCellIdentifier"
     let BusinessDetailTitleCellIdentifier        = "BusinessDetailTitleCellIdentifier"
     let BusinessDetailLocationCellIdentifier     = "BusinessDetailLocationCellIdentifier"
     let BusinessReviewTitleCellIdentifier        = "BusinessReviewTitleCellIdentifier"
  
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          navigationItem.title = business.name!
          
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     @IBAction func back(sender: AnyObject) {
          navigationController?.popViewControllerAnimated(true)
     }
     
     //TableView delegate
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return 4
     }
     
     func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
          
          //To refactor some day
          if indexPath.row == 0 {
               return 306
          }else if indexPath.row == 1{
               return 79
          }else if indexPath.row == 2{
               return 200
          }else if indexPath.row == 3{
               return 277
          }
          
          return 0
     }
     
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
          
          if indexPath.row == 0 {
              return businessPhotoCellAtIndexPath(indexPath)
          }else if indexPath.row == 1{
              return businessTitleCellAtIndexPath(indexPath)
          }else if indexPath.row == 2{
               return businessLocationCellAtIndexPath(indexPath)
          }
          
          return businessReviewTitleCellAtIndexPath(indexPath)
  
     }
     
     //return cell business thumb
     func businessPhotoCellAtIndexPath(indexPath:NSIndexPath) -> BusinessDetailThumbCell {
          let cell = tableView.dequeueReusableCellWithIdentifier(BusinessDetailThumbCellIdentifier) as! BusinessDetailThumbCell
          cell.businessThumbURL = business.image_url
          return cell
     }
     
     //return cell business title,rating...
     func businessTitleCellAtIndexPath(indexPath:NSIndexPath) -> BusinessDetailTitleCell {
          let cell = tableView.dequeueReusableCellWithIdentifier(BusinessDetailTitleCellIdentifier) as! BusinessDetailTitleCell
          cell.business = business
          return cell
     }
     
     //return cell business location
     func businessLocationCellAtIndexPath(indexPath:NSIndexPath) -> BusinessDetailLocationCell {
          let cell = tableView.dequeueReusableCellWithIdentifier(BusinessDetailLocationCellIdentifier) as! BusinessDetailLocationCell
          cell.business = business
          return cell
     }
     
     //return cell business Review Title map
     func businessReviewTitleCellAtIndexPath(indexPath:NSIndexPath) -> BusinessReviewTitleCell {
          let cell = tableView.dequeueReusableCellWithIdentifier(BusinessReviewTitleCellIdentifier) as! BusinessReviewTitleCell
          cell.business = business
          return cell
     }
     
    
     //Make a Call to business
     @IBAction func CallBusiness(){
          if let url = NSURL(string: "tel://\(business.phone!)") {
               UIApplication.sharedApplication().openURL(url)
          }
     }
     
     
}

