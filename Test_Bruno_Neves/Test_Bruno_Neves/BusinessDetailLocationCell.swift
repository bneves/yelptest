//
//  BusinessDetailSnippetTextCell.swift
//  Test_Bruno_Neves
//
//  Created by BRUNO NEVES on 04/05/16.
//  Copyright © 2016 BRUNO NEVES. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import CoreLocation

class BusinessDetailLocationCell: UITableViewCell,MKMapViewDelegate {
     
     @IBOutlet weak var businessLocation: UILabel!
     @IBOutlet var mapView: MKMapView!
     
     
     var business: Business!{
          didSet {
               self.initMapKit(business.location!)
          }
     }
     
     override func awakeFromNib() {
          super.awakeFromNib()
          self.contentView.clipsToBounds = true
          
          // Initialization code
     }
     
     override func setSelected(selected: Bool, animated: Bool) {
          super.setSelected(selected, animated: animated)
          
          // Configure the view for the selected state
     }
     
     
     func TraceRoute(lat:Double?,lon:Double?){
          let request = MKDirectionsRequest()
          request.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: UserInfo.latitude!, longitude: UserInfo.longitude!), addressDictionary: nil))
          request.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: lat!, longitude: lon!), addressDictionary: nil))
          request.requestsAlternateRoutes = true
          request.transportType = .Any
          
          let directions = MKDirections(request: request)
          
          directions.calculateDirectionsWithCompletionHandler { [unowned self] response, error in
               guard let unwrappedResponse = response else { return }
               
               for route in unwrappedResponse.routes {
                    self.mapView.addOverlay(route.polyline)
                    self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
               }
          }
          
          
     }
     
     func initMapKit(location:NSDictionary){
          
          var annotation:          MKAnnotation!
          var pointAnnotation:     MKPointAnnotation!
          var pinAnnotationView:   MKPinAnnotationView!
          
          if self.mapView.annotations.count != 0{
               annotation = self.mapView.annotations[0]
               self.mapView.removeAnnotation(annotation)
          }
          
          
          let latitude = location["coordinate"]!["latitude"] as! Double
          let longitude = location["coordinate"]!["longitude"] as! Double

          
          pointAnnotation = MKPointAnnotation()
          pointAnnotation.title = business.address
          pointAnnotation.coordinate = CLLocationCoordinate2D(latitude:latitude, longitude:longitude)
          
          
          pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: nil)
          
          
          self.mapView.centerCoordinate = pointAnnotation.coordinate
          self.mapView.addAnnotation(pinAnnotationView.annotation!)
          
          
         self.TraceRoute(latitude,lon: longitude)
          
     }
     
     func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
          let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
          renderer.strokeColor = UIColor.redColor()
          renderer.lineWidth = 2.5
          return renderer
     }
     
}

